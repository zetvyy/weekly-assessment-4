# Weekly-Asessment-FEClass : **WEEK 5**

**Deadline**: Friday, 5 November 2021, 23:59 WIB

## Instructions
1. Clone this project repository
2. Try to Convert Figma file to React (don't forget to installing CRA)
3. Following this [Link](https://www.figma.com/file/5Y47YXJgt2DoezlF7Y4Ytq/A-Simple-Landing-Page?node-id=0%3A1)
4. Push your result to your Personal repository
5. You can upload your link to in Our Classroom Assignment

## Task Requirements
- [ ] Unit using .rem / .em, don't use px
- [ ] Create 3 different screen

