import React from "react";
import "./Inputtext.css";

const InputText = text => {
  return (
    <form>
      <label>{text.label}</label>
      <br />
      <input type="text" />
    </form>
  );
};

export default InputText;
