import React from "react";
import "./main.css";
import InputText from "./../input/InputText";
import Button from "./../button/Button";

const Main = () => {
  return (
    <div className="box">
      <div className="left">
        <h1 className="header">
          got marketing? <br /> advance your <br /> business insight.
        </h1>
        <p className="paragraf">
          Fill out the form and receive our
          <br /> award winning newsletter.
        </p>
      </div>
      <div className="right">
        <div className="input">
          <div className="inputName">
            <InputText label="Name" />
          </div>
          <div className="inpuEmail">
            <InputText label="Email" />
          </div>
        </div>
        <div className="btn">
          <Button button="Sign me up" />
        </div>
      </div>
    </div>
  );
};

export default Main;
