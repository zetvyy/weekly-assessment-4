import React from "react";
import "./button.css";

const Button = button1 => {
  return (
    <button id="btn" type="submit">
      {button1.button}
    </button>
  );
};

export default Button;
